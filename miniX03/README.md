# MiniX03 - Dance while Loading 

![](/miniX03/Dance_while_Loading.png/)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX03)

[Click here to run the code](https://gitlab.com/gruppe-57431609/aesthetic-programming/-/blob/main/miniX03/miniX03sketch.js)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

# Description
The third MiniX-project was educational and differently because of the new tool about using loops and include a time-related function. My purpose was to create a colorful project and include an innovative throbber. 
Conceptually, the throbber design is the loading bar, which is designed to give visual feedback to the user. It consists of a loading bar that fills up gradually until the process is complete.  While the user is waiting for the loading bar to be full, then I have added another element. It is the dancing GIF that is interactive. In that way it the element make a entertain to the canvas  while the user is waiting for the loading process to be succes. Traditionally a throbber is placed in the middle of a canvas, which I would like to do differently. Another element is the two text “Dance while Loading…” and “(Click to Move Me)” which provides instructions to the user about the interface. All in all, the project consists of 3 elements: Throbber, GIF and Text.
Technically, the project is based on coding by using and create different piece of code. A Canvas setup, a dancing GIF, the background grid based on using loop, loading bar and Text.

# Reflection
**What do you want to explore and/or express?**
- I want to explore the traditional location for a throbber. I have always seen throbbers in the middle of a canvas and not being interactive, so I would like to explore with it in that way. My examination led me to end with a throbber that is placed down on a canvas because I want to give place to make the gif be playful for the user. The user can click anywhere on the canvas to entertain them self while the loading is in progress. Another important point I wanted to express was that the background does not have to be standard white and therefore I made it a very colorful and energetic to encourage the user to wait and be still in a positive mood.  
 
**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

- In my program I have used functions and function that are time related. The main time-related syntax/function is `millis()`, which returns the number of milliseconds since starting the sketch, when `setup()` is called.  
The background grid is drawn in the `draw ()` function by using the nested loop. The colors of the squares in the grid change randomly for each frame and creating a dynamic visual effect. It is maybe not directly related to time, but maybe more implicit. 
If we look at the animation of the loading bar, then it is controlled by the `drawLoadingBar() `function, which is called inside the `draw()` function. The `loadingBarWidth` is incremented by 1 for each frame until it reaches the `loadingBarMaxWidth`. This creates a nice effect and stimulate the progression of a loading process over time. 
If we look at the mouse interaction, `mouseClicked()` function, then it responds to mouse click events, which occur at specific points in time as the user interacts with the canvas. This function updates the position of the dancing GIF based on the mouse click coordinates, which is demonstrating the use of time-based events in user interaction.


**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**


- When I think about a throbber, then the first thing that comes into my mind is a line loading or a circle in the middle of the digital interface. Every social media has its own throbber and communicates to the user to wait, which can be impatient or entertaining.  A more concreate example could be a loading line, when you are posting a picture, story, or video on the social media e.g. Instagram. Another example could be on Facebook, where a round circle popping up on your screen when you want to load more and new posts.
All in all, the throbber is a symbol of human time vs. machine time/process, where it wants to show the user a sign of progress while the computer thinks/load something. It signifies that a process is ongoing where it is hiding the complexity of backend operations. Every throbber has a purpose on each digital interface. On the one hand we have throbbers that just show the passing time, but on the other hand there exist also throbbers that have some entertainment, as the dinosaur game when the user is waiting on Wi-Fi on Google. 
Designers characterize icons differently, where they could use all from symbolic representations to use interactive elements to provide feedback mechanisms.

# Reference
https://p5js.org/reference/#/p5/let

https://p5js.org/reference/#/p5/setup

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/mouseClicked

https://p5js.org/reference/#/p5/draw

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/text

https://p5js.org/reference/#/p5/textAlign

https://p5js.org/reference/#/p5/rect

https://p5js.org/reference/#/p5/mouseX

https://p5js.org/reference/#/p5/mouseY

https://p5js.org/reference/#/p5.Element/position

https://p5js.org/reference/#/p5/millis

https://www.youtube.com/watch?v=1c1_TMdf8b8&t=13s
