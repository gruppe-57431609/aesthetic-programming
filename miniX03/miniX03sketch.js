  // Initialize the variables
  let loadingBarWidth = 0;
  let loadingBarMaxWidth = 1000;
  let lastUpdateTime = 0;
  let updateInterval = 10; // updating the loading bar every 10 milliseconds

function setup() {
  createCanvas(1000, 500);
  background(0);

  // placing the gif in the center of canvas
  dancingGif = createImg('toothless-dancing.gif');
  dancingGif.size(200, 200);
  dancingGif.position(400, 130);
}

function draw() {
  background(0);
  strokeWeight(2);
  stroke(255);

  // drawing the backgroundGrid
  for (let y = 0; y < height; y += 20) {
    for (let x = 0; x < width; x += 20) {
      let r = random(200, 255); // Random value for red (200-255 for a lighter orange)
      let g = random(100, 200); // Random value for green (100-200 for a darker orange)
      let b = random(0, 50); // Random value for blue (0-50 for a darker orange)
      fill(r, g, b); // Use random RGB values to create an orange color
      rect(x, y, 20, 20);
    }
  }

  // updating the loading bar based on time
  if (millis() - lastUpdateTime > updateInterval) {
    drawLoadingBar();
    lastUpdateTime = millis(); // updating the last update time
  }

  // text "Dance while loading..." on canvas
  textSize(30);
  textStyle(BOLDITALIC);
  fill(255);
  textAlign(CENTER, TOP);
  text("Dance while Loading...", 500, 355);

  // text "(click to move me)" on canvas
  textSize(16);
  textStyle(BOLDITALIC);
  fill(255);
  textAlign(CENTER, BOTTOM);
  text("(Click to Move Me)", 500, 420);
}

  // loading bar
function drawLoadingBar() {
  stroke(0);
  fill(255, 0, 0);
  rect(0, height - 20, loadingBarWidth, 20);

  // Simulate loading by scaling the loading bar width
  if (loadingBarWidth < loadingBarMaxWidth) {
  // Call the transformational function to scale the loading bar width
    loadingBarWidth = scaleWidth(loadingBarWidth);
  }
}

  // Transformational function to scale the width
function scaleWidth(width) {
  return width + 1; // Incrementing the width by 1
}

  // placing a new gif where the mouse clicks on the canvas
function mouseClicked() {
  dancingGif.position(mouseX - 100, mouseY - 100);
}
