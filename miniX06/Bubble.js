class Bubble {
    constructor(x, y, r) {
      this.x = x;
      this.y = y;
      this.r = r;
      this.clicked = false;
    }
  
    // Method to check if a point is inside the bubble
    isClicked(point) {
      let d = dist(point.x, point.y, this.x, this.y);
      return d < this.r;
    }
  
    // Method to move the bubble
    move() {
      this.x = this.x + random(-2,2);
      this.y = this.y + random(-2,2);
    }

  // Method to display the bubble
    show() {
      stroke(255);
      strokeWeight(4);
      fill(0, 0, 255, 125);
      ellipse(this.x, this.y, this.r * 2);
    }
}
