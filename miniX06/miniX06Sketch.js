let bubbles = [];
let points = 0;
let timer = 5;
let r = 15;
let gameOver = false;
let img1, img2;

// Preload function to load images
function preload() {
  img1 = loadImage('ImgPlay1.png');
  img2 = loadImage('ImgGameOver2.png');
}

function setup() {
  createCanvas(600, 400);
  img1.resize(200, 0); 
  img2.resize(200, 0); 
}

function draw() {
  background(220);
  textSize(20);
  text(points, 20, 30);

  let player = createVector(mouseX, mouseY);

  // Generate new bubbles if timer is still running
  if (timer > 0) {
    for (let i = bubbles.length; i < 10; i++) {
      let x = random(width);
      let y = random(height);
      let radius = random(20, 60);
      let b = new Bubble(x, y, radius);
      bubbles.push(b);
    }
  }

  // Draw and handle bubbles
  for (let i = bubbles.length - 1; i >= 0; i--) {
    bubbles[i].show();
    if (bubbles[i].isClicked(player) && mouseIsPressed && !bubbles[i].clicked) {
      bubbles[i].clicked = true;
      points++;
      timer += 0.3;
    }
    if (bubbles[i].clicked) {
      bubbles.splice(i, 1);
    }
  }

  // The bubbles move
  for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].move();
    bubbles[i].show();
  }

  // Update timer
  if (timer > 0 && points > 0) {
    timer -= 1 / 60;
  }

  // Display timer
  let len = map(timer, 0, 5, 0, 200);
  rect(15, 50, 20, len);

  // Display "GAME OVER" when timer reaches 0
  if (timer <=0 && !gameOver) {
    gameOver = true;
    noLoop();
    textAlign(CENTER);
    textSize(50);
    fill(255, 0, 0);
    text("GAME OVER", width / 2, height / 2);
  }

  // Display the appropriate image
  if (!gameOver) {
    image(img1, 350, 170);
  } else {
    image(img2, 400, 200);
  }
}