# MiniX06 - Pop the Bubbles

![](/miniX06/popTheBubble.png/)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX06)

[Click here to run the code](https://gitlab.com/gruppe-57431609/aesthetic-programming/-/blob/main/miniX06/miniX06Sketch.js)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

# Description 
**Describe how does/do your game/game objects work?**
- The game consists of the main object, bubbles, that appear on the screen. The player must click on these bubbles to earn points before the timer runs out. In the game the bubbles move randomly around the canvas, and when clicked, they disappear, and the player earns a point and increase a little bit the time. The game ends when the timer reaches to zero, and a “GAME OVER” message is displayed. The player can retry the game again by refresh the window.


**Describe how you program the objects and their related attributes, and the methods in your game**

- The game objects are programmed using a `class-based` approach. Each `object`, bubble, is represented by the class called `Bubble`. This class defines attributes such as: `x (x-coordinate)`, `y (y-coordinate)`,` r (radius)`, and `clicked` (indicating whether the bubble has been clicked). Within the class includes methods like `isClicked` to check if a bubble is clicked by the player, `move()` to update the bubbles position, and `show()` to display the bubble on the screen. 


**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

- Object-oriented programming (OOP) simplifies complex system through encapsulation and abstraction. It highlights the essential aspects while hiding abstractions and irrelevant details. The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model. It is a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic.  In programming an object is a key concept, but it is also more generally understood as a thing with properties that can be identified in relation to the term subject. 

**Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**

- In the game “Pop the Bubbles”, the abstraction simplifies complex operations. Players focus on popping bubbles without worrying about the underlying complexities like bubble generation and score tracking. This makes me also reflect on an example about food delivery app. The users don’t need to concern about order routing or payment process because the focus is to increase the user-experience and efficiently by letting the user focus on simple goals as selecting their food and completing their transactions. It is allowing the user to focus on the basic rather than the technical details, which is why the complex details and operations are being abstracted. 

# Reference
Soon Winnie & Cox, Geoff, “Auto-generator”, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 6.

https://p5js.org/reference/

https://www.youtube.com/watch?v=auwYK02XC8U&t=96s

https://www.youtube.com/watch?v=TaN5At5RWH8

https://www.youtube.com/watch?v=tA_ZgruFF9k&t=33s

