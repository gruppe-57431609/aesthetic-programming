let heartColor; // Declare a variable to hold the color of the heart

  function setup() {
    // Put setup here
    // Creating a canvas filling the browser window
    createCanvas(windowWidth, windowHeight); 
    heartColor = color(255,0,0); 
  }

    // Random heart color on Mouse Click
  function mouseClicked() {
    heartColor = color(random(255), random(255), random(255)); 
  }

  function draw() {
    // Put drawing code here
    // Background sparkle effect
    background(0,25); 

    // Sparkle object with random location and size
    let sparkle = {
      locationX: random(width),
      locationY: random(height),
      size: random(1, 6)
    };

    fill(255); // The fill color for sparkle
    noStroke(); // Disables stroke for the sparkle
    ellipse(mouseX, mouseY, sparkle.size, sparkle.size); // Draws a sparkle at the mouse position
    ellipse(sparkle.locationX, sparkle.locationY, sparkle.size, sparkle.size); // Draws another sparkles at a random position
  
    // Drawing the heart with given location
    let x = width / 2; // Calculates the x-coordinate for the center of the canvas
    let y = height / 4; // Calculates the y-coordinate to position the heart closer to the top of the canvas
    let size = min(width, height) * 0.5; // Calculates the size of the heart based on the canvas dimensions
    drawHeart(x, y, size, heartColor); // Use the function to draw the heart at the specified position and size with the specified color
  }

    // Drawing the heart shape with given parameters
  function drawHeart(x, y, size, heartColor) {
    fill(heartColor); 
    beginShape(); 
    vertex(x, y + size / 4); // Defines the first corner of the heart
    bezierVertex(x + size / 2, y - size / 2, x + size, y + size / 4, x, y + size); // Defines the control and end points of the upper half of the heart
    bezierVertex(x - size, y + size / 4, x - size / 2, y - size / 2, x, y + size / 4); // Defines the control and end points of the lower half of the heart
    endShape(CLOSE); 
  }

