# MiniX01 - Sparkling Heart
 
![](/miniX01/ScreenshotMINIX01.png)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX01)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)
 

# Description
The first Mini-X project, we had the freedom to explore our own creativity, and I choose to create something simple and visually impactful, thus the lack of experience with coding, which motivated me to explore it and challenge myself. I had a passion to include the user into my project and create a minimalistic digital art. The miniX01, “Sparkling Heart”, is an interactive project consisting of a heart-shape object surrounded by sparkling particles that move and react to user interactions. It combines visual aesthetics with user involvement by clicking the heart, where its color changes dynamically and results a visual experience. The idea was inspirated by a combination of “The Coding Train” and the night sky when I was on a walking tour.  

**What have u produced?**

The process behind the “Sparkling Heart” consists of different tools from p5.js, where I played around the different Syntax’s to make it possible. It led me to use the following tools:
- `let heartColor` to variable declaration
- `function setup()` to setup function
- `createCanvas()` to canvas creation
- `heartColor()` to initial color assignment
- `function mouseClicked()` to mouse click event
- `heartColor=color( random(225), random(225), random(225))` to change color on mouse click
- `function draw()` to draw function
- `background(0,25)` to choose the background color


For the various shapes I have used following tools:
- `Ellipse()`
- Heart shape by using `beginShape()`, `vertex()`, `bezierVertex()` and `endShape(CLOSE)` functions within the `drawHeart()` function.

Firstly, I used the `let` function before creating my `setup()`, which permitted me to declare a variable to hold the color of the heart, also known as RGB. Subsequently, I could under the setup create a canvas filling the browser window by definding with the `createCanvas()` function that exist in the p5.js library. The `heartColor` is a variable with a specific color and represent the RGB value for red. But I wanted the heart to change the color every time the user click on the object, which led me to use another function called` mouseClicked()`. When the mouse is pressed and released, will the color of the heart change to a random color which is assigned to the heartColor variable. After the `setup()`, I could move on to the `draw() `and beginning to create the sparkling background and heart. 

Firstly, the sparkles on the background were in focus. The `background()` is semi-transparent because I wanted to get the best sparkly effect and then defined an object representing a sparkle with random location. Each sparkle is represented as an object with properties for its position by using `locationX` and `locationY`, and the size by using `size`, which is randomized between 1 and 6. The aesthetical side of the sparkles are drawn as white ellipses with `noStroke`, where I on the one hand had created the sparkle to follow the mouse position, while I on the other hand had created the sparkle to appear at a random position on the canvas.

Secondly, the heart was in focus. The shape of the heart needs to appear on the canvas. In the beginning, the variables x and y are calculated to position the heart at the center of the canvas and then determined the size. After I got it in place, I started shaping the heart with a closing function called `endShape()`, and adding color (`fill()`) .


# Reflection
**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

I would describe my first independent coding experience as special, interesting, challenging, hard and successful. “Where to begin?” was my first thought when I opened my device. It was all from getting an idea to do a short recap about the program or coding, and then go to the reference over and over, until u get a flow. It was a challenging beginning but when you find a flow then you feel great and gets a special experience. A good slogan from me, that is in my mind is “errors and mistakes is good" because in the end I will find the key, I had looking for and solve the mistakes.

**How is the coding process different from, or similar to, reading and writing text?**

The coding process is different from reading and writing text. If we look at the writing process for coding and text, then it is different. When we are writing code, we had to be specific because we are giving instructions to a computer, which is fragile to mistakes and often will lead to errors, e.g. a blank page because the computer doesn’t understand the human language. If we are writing text, then we focus to communicate with the human audience by implementing another style, purpose, or method. We can always add explanation because the writing is more flexible than a computer that needs a high level of precision. In the end we can also see similarities between them. Both make the chance and opportunities to involve creativity, but the structure and grammar rules is similar too. 
The reading is different beacuse they both has their own language, and therefore communicate differently. The coding are more about logical instructions for computers, while reading texts focuses on understanding written information.

**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

Code and programming have always been interesting for me. I am not professional, but the assigned readings have helped me to understand and develop in the correct direction to feel enough comfortable in the end about coding and programming. The book: “Aesthetic programming: A Handbook of Software Studies” (2020) by Winnie Soon Geoff Cox is an example of a great tool to understand it better. All in all I am looking forward to learn more about it!


# Reference
https://p5js.org/reference/#/p5/let

https://p5js.org/reference/#/p5/setup

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/color

https://p5js.org/reference/#/p5/mouseClicked

https://p5js.org/reference/#/p5/draw

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/noStroke

https://p5js.org/reference/#/p5/random

https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/beginShape

https://p5js.org/reference/#/p5/vertex

https://p5js.org/reference/#/p5/bezierVertex

https://p5js.org/reference/#/p5/endShape

https://www.youtube.com/watch?v=oUBAi9xQ2X4&t=444s

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
