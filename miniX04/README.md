# MiniX04 - Google's Data Dystopia

![](/miniX04/googlesDataDystopia.png/)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX04)

[Click here to run the code](https://gitlab.com/gruppe-57431609/aesthetic-programming/-/blob/main/miniX04/miniX04Sketch.js)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

# Description
"Google’s Data Dystopia" is a work of art that reveals the part of history exploitation most of us often don’t think about. This work of art sheds light on Google's exploitation of user data through a simulated search engine experience. It addresses the seriousness of giving access knowingly or unknowingly to big tech giants about one’s data history and the consequences this can bring. As characters are entered, ominous messages such as: “Ssssshh… I can remember your web searches” or “Thanks… I can capture your information” appear, illustrating the invasive nature of data collection. If the reset trigged, an error message backdrop emerges, while hitting enter wipes the screen. This interactive art explores the unsettling reality behind the façade of open infrastructure, where users unknowingly trade their personal information for convenience. The awareness of fostering data privacy issues and ethical considerations is therefore important.

**Describe your program and what you have used and learnt**

The program of my miniX04 is a reflecting message about the exploitation of user data. It consists firstly of the users input by triggering characters and results in dynamic responses, that highlights the hidden data collection.  Secondly, a reset button prompts an error backdrop, which symbolize the consequences, while thirdly is the press of the enter, that clears the screen, representing data erasure. In the program, I mostly worked with `button`, `arrays`, `mousePressed`, `keyPressed` and `nested loop`. I have made a canvas, which is `windowWidth` and `windowHeight` in white `background`, but can end in a canvas manipulation by pressing the reset-button.  The program is also structured with `if-else statements` that controls at which and how point certain elements appear on screen. All in all, my purpose was to experiment the new theory about data acquisition inputs and interactive devices, which I did by experimenting and include in my project with mouse and keyboard. It was a little bit challenging but interesting and ended up with a successful project.

- Keyboard = the interaction will result to a ominous messages.
- Reset button = An aesthetic error message appear.
- Enter button = wipes the screen.


**Articulate how your program and thinking address the theme of “capture all.”**

The program embodies the theme of "Capture all" by simulating a Google search interface where user interactions symbolize data capture. It forces them to reflect about the data being captured when they don’t change their history settings against the tech-giants. I wanted to highlight how much web searches can actually exploit your privacy when you don’t change or delete your history after the searches.  The ability to reset the canvas underscores the protective privacy, but also emphasizing the challenges of escaping data capture. It might be a bit abstract version of the program but a great wakeup call and enlighten you to think deeper before searching.

**What are the cultural implications of data capture?**

Most of us don't think about questions such as which kinds of data Is being captured and how it is being processed? These questions should maybe be an important reflection to consider, but as well also the term datafication (Soon & Cox, 2020). Framed under the concept of "datafication," our lives are increasingly transformed into data points that are processed, monetized, and used for various purposes. This normalization of surveillance capitalism,” the patterns of human behaviors,” raises concerns about privacy and influences e.g. the social norms. Overall, the cultural implications of data capture underscore the importance of critical thinking, ethical considerations, and active regulation to ensure data capture practices align with democratic values, individual rights are respected, and social welfare is promoted.

# Reference
https://p5js.org/reference/#/p5/let

https://p5js.org/reference/#/p5/setup

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/draw

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/text

https://p5js.org/reference/#/p5/textAlign

https://p5js.org/reference/#/p5/keyPressed

https://www.youtube.com/watch?v=1c1_TMdf8b8&t=13s

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

https://www.dr.dk/nyheder/viden/tech/video-saadan-sletter-du-hvad-google-husker-om-dig 

https://www.wired.com/story/google-tracks-you-privacy/ 



