// array of sentences to display on canvas
let sentences = [
  "WRITE",
  "Ssssshh...",
  "Ssssshh... I can remember your web searches",
  "Ups... I can track you",
  "Ups... I can see your location",
  "Thanks... I can capture more information",
  "Ssssshh... I can use it",
  "Ssssshh... I can forward it ",
  "Please... Give me more",
  "YES... Give me more",
  "YES",
  "Ssssshh... I can learn your algorithm.",
  "Ssssshh... I can steal it",
  "Uuuuuh",
  "Sorry, do not write if you want to keep the privacy"
];

// defining an array of error colors
let errorColors = ["green", "blue", "red", "yellow", "black"]; 

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);

  // placing the GOOGLE-image in a specific position
  googleImg = createImg('googlesearch.png');
  googleImg.size(400, 300);
  googleImg.position(550, 200);
  
  // creating a button
  let resetButton = createButton('Reset');
  resetButton.position(720, 460);
  resetButton.mousePressed(resetCanvas);
}

function draw() {
   // text "Search..." on canvas
   textSize(25);
   fill(200);
   textAlign(CENTER, TOP);
   text("Search...", 680, 385);
}

function resetCanvas() {
  // clear the canvas by drawing a background
  background(255);
  
  // drawing the background grid
  strokeWeight(2);
  stroke(255);
  for (let y = 0; y < height; y += 20) {
    for (let x = 0; x < width; x += 20) {
      let colorChoice = random(errorColors);
      fill(colorChoice);
      rect(x, y, 20, 20);
    }
  }
}

function keyPressed() {
  if (keyCode === ENTER) {
    // clear the canvas by drawing a background
    background(255);
  } else if ((key >= 'a' && key <= 'z')) {
    let randomX = random(windowWidth);
    let randomY = random(windowHeight);
    let randomSentence = random(sentences);
    
    fill(random(255), random(255), random(255));
    text(randomSentence, randomX, randomY);
  }
}
