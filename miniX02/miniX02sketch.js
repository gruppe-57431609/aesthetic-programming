// definding an object with each property represents a candle and the corresponding value represents an emotion
let words = {
  candle1: 'Cringe',
  candle2: 'Freezing',
  candle3: 'Nervous',
  candle4: 'Grimacing',
};

// declare and initialize two variables
let selectedWord = '';
let wordPosition;

function setup() {
  createCanvas(1000, 500);
  background(220, 220, 250);
  textFont('Courier New');
}

function draw() {
  drawPrompt();
  drawEmoji1();
  drawCandles();
  drawWord();
}

function drawPrompt() {
  fill(0);
  textSize(24);
  textAlign(CENTER, TOP); 
  text("How do you interpret this emoji?", width / 2, 40); 
  text("(Click the candles)", width / 2, 70)
}

function drawEmoji1() {
  // Emoji head
  noStroke();
  fill(250, 200, 70);
  ellipse(width / 2, height / 2, 250);

  //EYE 1
  fill(100);
  ellipse(450, 205, 45);

  //EYE 2
  //use same values for y and (width - eye1 x) for x position
  ellipse(width - 450, 205, 45);

  //MOUTH
  fill(255);

  //rectangle
  rect(430, 270, 140, 55, 20);

  //line - use stroke() instead of fill for color
  stroke(100);
  strokeWeight(4);
  line(570, 300, 431, 300);
}

function drawCandles() {
  // candle 1
  noStroke();
  fill(255);
  rect(100, 300, 20, 200);
  fill(255, 204, 0);
  triangle(110, 200, 99, 150, 120, 150);

  // candle 2
  noStroke();
  fill(255);
  rect(270, 300, 20, 200);
  fill(255, 204, 0);
  triangle(280, 200, 270, 150, 290, 150);

  // candle 3
  noStroke();
  fill(255);
  rect(700, 300, 20, 200);
  fill(255, 204, 0);
  triangle(710, 200, 700, 150, 720, 150);

  // candle 4
  noStroke();
  fill(255);
  rect(880, 300, 20, 200);
  fill(255, 204, 0);
  triangle(893, 200, 903, 150, 885, 150);
}

function drawWord() {
  if (selectedWord !== '') {
    fill(0);
    textSize(24);
    textAlign(CENTER, CENTER);
    text(selectedWord, wordPosition.x, wordPosition.y); //Display the selected word at the specified position
  }
}

function mouseClicked() {
// checks which candle is clicked by calling 'checkCandle()' with specific coordinates and candle identifiers.
  checkCandle(100, 120, 'candle1');
  checkCandle(270, 290, 'candle2');
  checkCandle(700, 720, 'candle3');
  checkCandle(880, 900, 'candle4');
}

function checkCandle(xMin, xMax, candle) {
// checks if a mouse-click occurs within the bounds of a specific candle area. If it is, it updates 'selectedWord' and 'wordPosition'.
  if (mouseX > xMin && mouseX < xMax && mouseY > 100 && mouseY < 500) {
    selectedWord = words[candle];
    wordPosition = createVector((xMin + xMax) / 2, 250);
  }
}
