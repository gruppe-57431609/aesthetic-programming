# MiniX02 - Emoji and Candles

![](/miniX02/Emoji_and_candles.png/)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX02)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

# Description
**Describe your program and what you have used and learnt**

The second MiniX-project was very interesting because of the new context, where the two emoji must be placed in a wider social and cultural perspective that concerns a politics of representation, identity, race, colonialism, or something else. I got my inspiration from different articles about, how the emojis can be interpret in different ways. The emojis are full of double meanings and are perceived differently across culture, gender, and age. Everyone creates their own and multifaceted interpretations for each emoji they use. One person interprets it one way, but the person across from you may interpret it differently, which means you must be careful which emoji you use in which context. This was very interesting for me to read these different articles about it and lead me to the final idea. The miniX02, “Emoji and Candles”, is an interactive project, where the main components include an emoji, four candles, and a text prompt. Users can click on one of the candles-emoji to examine what interpretations this emoji can have. 
I have used different shapes, geometries, and other related syntax, but kept it simple. The process behind the “Emoji and Candles” consists of different tools from p5.js library, where I played around the different Syntax’s to make it possible. It led me to use e.g. the following tools: 
-	`Let` – statements to variable declaration
-	`Function setup()` to setup function
-	`CreateCanvas()` to canvas creation
-	`Function draw()` to draw function
-	`Function mouseClicked()` to mouse click event
-	`Function drawPrompt()` to choose the main typography and texttype

I have created two different emojis. The first emoji is a grimacing emoji, which consist of various shapes with following tools: `ellipse()`, `rect()` and `line()`, and also worked with both `strokeWeight()`, `fill()` and `stroke()` to end with the desired look of the first emoji. The second emoji is four candles, which consist of shapes as: `rect()` and `triangle()` with `fill()` and every candle with `noStroke()`. I had also learned, how to use the `if()` and also decided to use a new tool as the function `text()` to include a text but also to choose the perfect typography. 


# Reflection
**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it)**

The assignment was to create two emoji that experiment with various geometric and shapes, but also to reflect on the politics/cultural context. I would like to express how emoji have many different interpretations. It can be understood in many ways precisely because humans are not the same and have different cultural backgrounds and perceptions. I have chosen to make an emoji that is popular to use and is one of those emojis that can be interpreted in many ways. Interpretation as cringe, freezing, nervous, and grimacing. The second emoji is the 4 candles, which also have different interpretations, but have chosen the first emoji to be the one to focus on the most. The candles symbolize, i.e. enlightening something, which fits in good with my project to highlight the interpretations people can have about the first emoji. Gender, age and culture have an effect on how we as receivers read the sender's message. 

# Reference
https://nyheder.tv2.dk/tech/2016-04-13-det-ser-maaske-uskyldigt-ud-men-det-er-det-slet-ikke

https://videnskab.dk/kultur-samfund/glad-eller-aggressiv-vi-opfatter-emojis-forskelligt/

https://soundvenue.com/kultur/2015/06/ni-kontroversielle-emojis-der-har-skabt-undren-og-splid-152299

https://analog.nu/2019/04/14/tolker-du-emojis-paa-samme-maade-som-dine-venner/

Crystal Abidin and Joel Gn, eds., "Histories and Cultures of Emoji Vernaculars," First Monday 23, no. 9, September (2018), https://firstmonday.org/ojs/index.php/fm/issue/view/607.

https://p5js.org/reference/#/p5/let

https://p5js.org/reference/#/p5/setup

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/mouseClicked

https://p5js.org/reference/#/p5/draw

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/noStroke

https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/text

https://p5js.org/reference/#/p5/textAlign

https://p5js.org/reference/#/p5/triangle

https://p5js.org/reference/#/p5/rect

https://p5js.org/reference/#/p5/mouseX

https://p5js.org/reference/#/p5/mouseY 

