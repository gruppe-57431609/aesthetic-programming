# MiniX05 - Cosmos & Chaos

![](/miniX05/Cosmos.png/)
![](/miniX05/Chaos.png/)

[Click here to run the program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX05)

[Click here to run the code](https://gitlab.com/gruppe-57431609/aesthetic-programming/-/blob/main/miniX05/miniX05Sketch.js)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

# Description 
**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

The program generates a pattern of concentric circles on the canvas with the following rules:

- **Clear the Canvas and Begin:** Always begin with a clear canvas.
- **Orderly drawing:** The program orderly draws concentric circles on the canvas, moving from left to right and top to bottom.
- **Using random colors with transparency**
- **Transition of Chaos Mode:** Once the circles reach the bottom of the canvas, then clear the canvas. The program transition to chaotic movement. Here will the circles move randomly in both horizontal and vertical directions. 
- **Refreshing to Cosmos State:** As soon the user clicks the refresh icon, then get back to Cosmos state. 


The program performs over time by switching between orderly movement and chaotic movement. The orderly drawing creates structured patterns until the circles reach the bottom of the canvas and switch to the chaotic movement. The chaotic movement introduces randomness, which leads to an emergent interplay of order and chaos. Emergent behavior arises as structured patterns are disrupted by randomness, and result in dynamic and evolving patterns on the canvas. The syntaxes `x += spacing` and `y += spacing` increase the `x and y coordinates` to move the pattern forward and create a new row when the circles reach the edge of the canvas. The `for-loop` controls the orderly drawing of concentric circles, and it iterates each row until it reaches its minimum size. Afterwards it will get into a chaotic mode, where the `x and y coordinates` changes randomly, and creates an emergent character.

**What role do rules and processes have in your work?** 

The rules and processes have a role in the project because it shapes the behavior and appearance of the generative program, where it allows to create the complex and dynamic patterns on the canvas. In this work we establish rules and the process to control the program, but what will happen if there were no rules?  Would the randomness be involved more? The less constraints, the more freedom and randomness the program will have. The project begins with order and rules how the program should run the cosmos, but as soon it reaches to the bottom, then it is free and increase the randomness. The code uses a `for-loop` to draw concentric circles and `if-statements` to control the flow of the program.

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

The Idea of _“auto-generator”_ makes sense through my miniX. The auto-generator is the generative program, where the programmer has created it by using p5.js. The programmer has the control over the generative process through the establishment of rules. If the programmer defines the behavior and patterns, then it will exert a level of control over the output of the generative program. Is it about the balance of a programmer and a computer? According to Sol Le Witt’s, then it is _“The idea becomes a machine that makes the art” (Sol LeWitt, 1997)_. The programmer set the rules, but the program operates independently to create the output over time, which makes it to be a shared process. Maybe if the programmer includes more boundaries and rules into the program, the more will the program be controlled and less creative. We make the creation of art through the program. Perhaps it is better understood by including a comparison with _“the king’s successor”_ as a metaphor for my point. The art is based on a set of instructions, where the outcome might be different because it depends on how the instructions are interpreted. In connection with that, one could perhaps think about how sensible it actually can be?

# Reference
https://p5js.org/reference/#/p5/let

https://p5js.org/reference/#/p5/setup

https://p5js.org/reference/#/p5/createCanvas

https://p5js.org/reference/#/p5/draw

https://p5js.org/reference/#/p5/background

https://p5js.org/reference/#/p5/fill

https://p5js.org/reference/#/p5/random

https://p5js.org/reference/#/p5/ellipse

https://p5js.org/reference/#/p5/text

https://p5js.org/reference/#/p5/textAlign

https://p5js.org/reference/#/p5/textSize

Sol LeWitt cited in Lippard, ed. Six Years: The Dematerialization of the Art Object from 1966 to 1972. 

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
