let x = 0;
let y = 0;
let spacing = 40;
let maxSize = 100;
let minSize = 20;
let isChaos = false; // to indicate if chaos mode is active

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  stroke(0);
  textAlign(CENTER, CENTER); // Align text to center
  textSize(100); // Set text size
}

function draw() {
  // Display text based on mode
  if (!isChaos) {
    fill(255); // White color for text
    text("In The Dance Of The Cosmos", width / 2, height / 2); // Display "Cosmos" in the center
  } else {
    fill(255); // White color for text
    text("Chaos Whispers Its Secrets", width / 2, height / 2); // Display "Chaos" in the center
  }

  // Generating random color and transparency
  let r = random(255);
  let g = random(255);
  let b = random(255);
  let alpha = random(50, 200); 

  // Seting fill color with random properties
  fill(r, g, b, alpha); 

  // Draw concentric circles
  for (let size = maxSize; size >= minSize; size -= 10) {
    ellipse(x + spacing / 2, y + spacing / 2, size, size);
  }

  x += spacing;
  if (x > width) {
    x = 0;
    y += spacing;
  }

  // Reset y position and clear canvas when reaching the bottom
  if (y > height) {
    y = 0;
    background(0); // Clear canvas
    isChaos = true; // Activate chaos mode
  }

  // if chaos mode is active, add chaotic behavior
  if (isChaos) {
    x += random(-spacing, spacing); // random move horizontally
    y += random(-spacing, spacing); // random move vertically
  } 

}

