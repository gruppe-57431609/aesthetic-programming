# MiniX09 - Flowchart
Among all the miniX-projects, MiniX06 "Pop The Bubbles" seems to be the most complex to represent in a flowchart. It involves multiple components such as generating and handling bubbles, updating timer, displaying images and more. Here is a flowchart representing the process of the projects:

![](/miniX09/Flowchart.png/)

[Click here to run the miniX06 program](https://gruppe-57431609.gitlab.io/aesthetic-programming/miniX06)

[Click here to run the miniX06 code](https://gitlab.com/gruppe-57431609/aesthetic-programming/-/blob/main/miniX06/miniX06Sketch.js)

[Click here to view the repository](https://gitlab.com/gruppe-57431609/aesthetic-programming.git)

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

It is difficult to create a flowchart explaining a process/flow of a program as you need to decide which elements, decisions and processes that need to be included. Some may not seem important but may actually be when explaining the processes executed within a running program. It can also be difficult to figure out where the elements in the flowchart need to be linked and especially when you need to create a new step in the process or if the process only can be repeated. 

**What are the technical challenges facing the two ideas and how are you going to address these?**

The first Idea is called “(Not so) Safe Data”, which is about a website that you don’t trust 100 precent. However, if you enter the information and do not check the website to see if it is good enough to trust, you risk some consequences that could be harmful. Often at the bottom of a website it says what the owner doesn't want you to read but still must be there, some other information that you can unconsciously approve that it must take. Therefore, is our purpose: Read the information/text in the fine print. If the user approves the information, you will be attacked by viruses and be hacked unless you find the stop button and click on it. The technical challenges we are facing here is that the idea includes many elements and function that needs to cooperate in order to achieve the desired goal. 

![](/miniX09/FlowchartSafe.png/)
 
The second idea is called “Blackout”, which is about alcohol and has the purpose to show what happens if you increase the alcohol use. It can be difficult to visualize as there are a lot of elements involved. The prospect of the BAC having to increase, and decrease involves a lot of different choices that all must be visualized which is why the flowchart might look complicated at first sight. However, both the intake of liquid and food work in the same way as the intake of water can decrease your BAC just like eating some food would decrease it. Then the timer which works in the background is also an extra element, however, its main job is to help decrease the BAC as time passes, the same way it would in real life, and simply runs in the background without adding any extra technical difficulty to the code. The code would end up using a lot of the same functions to create these elements which means that the chart looks a lot more complicated than it would be in practice.

![](/miniX09/FlowchartBlack.png/)

**In which ways are the individual and the group flowcharts you produced useful?**

Both the individual and the group flowcharts are useful. The individual flowcharts are useful for personal understanding, where it gives the person a better perspective for their program and process. It is also a great training tool/method, and a tool that Is good for documentation and explanation to an outsider, which can simplify it and do it less complex. The clarity of a program can be very high with a flowchart. 
On the other hand, the flowcharts are also useful for communication, discussion, and collaborative planning. A method that can be used to plan a starting point or brainstorming, but mostly to make a overview of the system in a program.



